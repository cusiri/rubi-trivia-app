// The cloud functions for firebase SDK to create Cloud functions and set up triggers

//TODO Must refractor and use proper callback procefure. Promises etc

const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

var db = admin.firestore();

//firestore triggers
exports.createUser = functions.firestore
	.document('game_users/{userId}')
	.onCreate((snap,context) => {
		const userValue = snap.data();
		const referer = userValue.ref;
		console.log(referer + "The UID of the users that refered me");
		getRefererInformationFromFirestore(referer);
	});



// Methods
function getRefererInformationFromFirestore(uid){
	var userRef = db.collection('game_users').doc(uid);
	var getdoc = userRef.get()
		.then(doc => {
			if(!doc.exists){
				console.log('doc does not exist');
			}else{
				console.log('doc exists');
				// Write code here
				const refVal = doc.data();
				const refEl = refVal.el; // Boolean value
				
				//check to see if the user has an extra life
				if(!refEl){
				// Users does not have extra life, so we give them an extra life
				updateRefererWithExtraLife(uid);
				}
			}
			return doc.data();
		}).catch(err => {
			console.log('Err getting doc',err);
		})
}

function updateRefererWithExtraLife(uid){
	var refererRef = db.collection('game_users').doc(uid);
	var refEl = refererRef.update({el:true});
}






