package com.quiz.calvinusiri.triviaapp.Utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.quiz.calvinusiri.triviaapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class LeaderboardRecyclerViewAdapter extends FirestoreRecyclerAdapter<Users, LeaderboardRecyclerViewAdapter.NoteHolder> {

    public LeaderboardRecyclerViewAdapter(@NonNull FirestoreRecyclerOptions options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull NoteHolder holder, int position, @NonNull Users model) {
        holder.amountTextView.setText(String.valueOf(model.getAw()));
        holder.rankTextView.setText(String.valueOf(position+1));
        holder.userTextView.setText(model.setEmailAsReference(model.getE()));
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user_cardview_leaderboard_view,parent,false);
        return new NoteHolder(view);
    }

    class NoteHolder extends RecyclerView.ViewHolder{
        private View view;
        private TextView rankTextView;
        private TextView userTextView;
        private TextView amountTextView;
        CardView parentLayout;
        public NoteHolder(View itemView) {
            super(itemView);
            rankTextView = itemView.findViewById(R.id.layout_user_leaderboard_view_rank_text_view_id);
            userTextView = itemView.findViewById(R.id.layout_user_leaderboard_view_text_view_name_id);
            amountTextView = itemView.findViewById(R.id.layout_user_leaderboard_view_amount_made_id);
            parentLayout = itemView.findViewById(R.id.layout_user_leaderboard_view_id);
        }
    }
}
