package com.quiz.calvinusiri.triviaapp.Controllers.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.HideKeyboard;
import com.quiz.calvinusiri.triviaapp.Utils.ServerLookUpFunctions;
import com.squareup.haha.perflib.Main;

public class LoginMainActivity extends AppCompatActivity {
    private RelativeLayout mainLayout;

    private TextView legalTextView;
    private TextView signUpButton;
    private Button loginButton;

    private EditText email;
    private EditText password;

    private ProgressDialog mProgress;

    public  Handler checkIfUserIsLoggedInHandler;
    private Handler checkToSeeIfUserIsSignedInLoginMainActivityHandler;

    private static final String TAG = "LoginMainActivity";
    private Context mContext = LoginMainActivity.this;

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        ServerLookUpFunctions.serverLookUpIfSignedIn(checkToSeeIfUserIsSignedInLoginMainActivityHandler);// Checks to see if the user is signed up
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);

        mainLayout = findViewById(R.id.login_main_layout_id);
        signUpButton = findViewById(R.id.activity_login_main_text_view_sign_up_id);
        legalTextView = findViewById(R.id.activity_login_main_text_view_tnc_id);
        loginButton = findViewById(R.id.activity_login_main_button_login_id);
        email = findViewById(R.id.login_email_edt_text_email);
        password = findViewById(R.id.login_email_edt_text_password);
        mProgress = new ProgressDialog(this);

        //Methods
        hideKeyboard();// Hide Keyboard
        setUpProgressDialog();
        setUpFullScreenView();

        setUpOnClickListener();

        handleCallBack();
    }

    private void handleCallBack(){
        // Handler that checks to see if the user exists in the DB and signs them in
        checkIfUserIsLoggedInHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.arg1 == 1){
                    // Signin successful
                    navigateActivities(mContext,MainMenuMainActivity.class);
                    mProgress.dismiss();
                }else{
                    //Signin failed
                    email.getText().clear();
                    password.getText().clear();
                    mProgress.dismiss();
                    Toast.makeText(mContext,String.valueOf(getText(R.string.bad_login)),Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });
        // Handler to check to see if the user is logged in or not
        checkToSeeIfUserIsSignedInLoginMainActivityHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.arg1 == 1){
                    // If Already signed in, segue to main activity
                    navigateActivities(mContext,MainMenuMainActivity.class);
                }
                return false;
            }
        });
    }

    private void setUpOnClickListener(){
        // onClickListenners
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateActivities(mContext, SignUpMainActivity.class);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailString = email.getText().toString().trim();
                String passwordString = password.getText().toString().trim();
                if(!emailString.isEmpty() && !passwordString.isEmpty()){
                    mProgress.show();
                    ServerLookUpFunctions.serverLookUpLogIn(checkIfUserIsLoggedInHandler,emailString,passwordString);
                }else{
                    Toast.makeText(mContext,String.valueOf(getText(R.string.empty_fields)),Toast.LENGTH_LONG).show();
                }
            }
        });

        legalTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateActivities(mContext,LegalMainActivity.class);
            }
        });

    }

    private void hideKeyboard(){
        HideKeyboard keyboard = new HideKeyboard(LoginMainActivity.this);
        keyboard.setupUI(mainLayout);
    }

    private void setUpProgressDialog(){
        if(mProgress != null){
            mProgress.setTitle("Processing...");
            mProgress.setMessage("Please wait...");
            mProgress.setCancelable(false);
            mProgress.setIndeterminate(true);
        }
    }

    private void setUpFullScreenView(){
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void navigateActivities(Context currentContext, Class movingClass){
        if(currentContext != null){
            Intent intent = new Intent(currentContext,movingClass);
            startActivity(intent);
            finish();
        }
    }

}
