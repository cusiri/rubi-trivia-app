package com.quiz.calvinusiri.triviaapp.Controllers.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.HideKeyboard;
import com.quiz.calvinusiri.triviaapp.Utils.ServerReceiveFromDB;

import java.util.HashMap;
import java.util.Map;

public class PayPalFragmentController extends Fragment {
    private Button cashOutButton;

    private EditText emailEditText;
    private EditText phoneNumberEdtiText;

    private RelativeLayout mainLayout;

    public Handler PayPalHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paypal_layout,container,false);
        /*
         * For now will send an email but looking to set up a server to auto send money
         * */
        cashOutButton = view.findViewById(R.id.fragment_paypal_cashout_button_id);

        emailEditText = view.findViewById(R.id.fragment_paypal_email_edit_text_id);
        phoneNumberEdtiText = view.findViewById(R.id.fragment_paypal_phone_number_edit_text_id);

        mainLayout = view.findViewById(R.id.fragment_paypal_layout_main_layout_id);

        PayPalHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                Map usersMap  = (HashMap) message.obj;
                if(usersMap != null){
                    Users user = (Users) usersMap.get("user");
                    String emailMessage = "Email: " + emailEditText.getText().toString().trim() + "/n Full Name: " + user.setEmailAsReference(user.getE())
                            + "/n Phone:" + phoneNumberEdtiText.getText().toString().trim() +"/nUID: "+usersMap.get("uid");
                    sendEmailToCashOutMoney(emailMessage);
                }else{
                    Toast.makeText(getContext(),String.valueOf(getText(R.string.poor_internet)),Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        if (isAdded()){
            setUpOnClickListeners();
            hideKeyboard();
        }


        return view;
    }
    private void setUpOnClickListeners(){
        cashOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(phoneNumberEdtiText.getText()) && !TextUtils.isEmpty(emailEditText.getText())){
                    ServerReceiveFromDB.receiveLoggedInUserInformationFromServer(PayPalHandler);
                }else{
                    Toast.makeText(getContext(), "Fields can't be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void sendEmailToCashOutMoney(String message){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"calvin.frank.usiri@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "PAYPAL CASH ME OUT PLEASE");
        i.putExtra(Intent.EXTRA_TEXT   , message);
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void hideKeyboard(){
        HideKeyboard keyboard = new HideKeyboard(getActivity());
        keyboard.setupUI(mainLayout);
    }
}
