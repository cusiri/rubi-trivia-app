package com.quiz.calvinusiri.triviaapp.Model;

public class GameInformation {
    private String gamePrize;
    private String gameDate;
    private String gameStatus;
    private String gameTime;
    private String gameId;

    GameInformation(){}
    public GameInformation(String nextGameTime, String nextGamePrizeMoney, String gameDate,String gameStatus
    , String gameId) {
        this.gameDate =
        this.gameTime = nextGameTime;
        this.gamePrize = nextGamePrizeMoney;
        this.gameDate = gameDate;
        this.gameStatus = gameStatus;
        this.gameId = gameId;
    }

    public String getGamePrize() {
        return gamePrize;
    }

    public String getGameDate() {
        return gameDate;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public String getGameTime() {
        return gameTime;
    }

    public String getGameId() {
        return gameId;
    }
}
