package com.quiz.calvinusiri.triviaapp.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class FragmentSectionsPagerAdapter extends FragmentPagerAdapter {
    //Keeping track of fragments
    private final List<Fragment> mFragmentList = new ArrayList<>();

    public FragmentSectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    // Used to add fragments to adapter
    public void addFragment(Fragment fragment){
        mFragmentList.add(fragment);
    }
}
