package com.quiz.calvinusiri.triviaapp.Controllers.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.quiz.calvinusiri.triviaapp.Controllers.Fragments.MobileMoneyFragmentController;
import com.quiz.calvinusiri.triviaapp.Controllers.Fragments.PayPalFragmentController;
import com.quiz.calvinusiri.triviaapp.Controllers.Fragments.PickUpCashFragmentController;
import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.FragmentSectionsPagerAdapter;
import com.squareup.haha.perflib.Main;

public class CashOutMainActivity extends AppCompatActivity {
    private ImageView backButton;
    private TextView navBarTitle;

    private Context mContext = CashOutMainActivity.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_out_main);
        backButton = findViewById(R.id.snipet_nav_bar_back_button_id);
        navBarTitle = findViewById(R.id.snipet_nav_bar_title_id);

        setUpOnClickListerners();
        setUpViewPager();
        setUpUi();
    }

    private void setUpViewPager(){
        FragmentSectionsPagerAdapter adapter = new FragmentSectionsPagerAdapter(getSupportFragmentManager());
        if(adapter != null) {
            adapter.addFragment(new PayPalFragmentController());
            adapter.addFragment(new MobileMoneyFragmentController());
            adapter.addFragment(new PickUpCashFragmentController());

            ViewPager viewPager = findViewById(R.id.activity_cash_out_main_view_pager);
            viewPager.setAdapter(adapter);

            TabLayout tabLayout = findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_paypal);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_mobile_money);
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_pick_up);
        }

    }
    private void setUpOnClickListerners(){
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MainMenuMainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void setUpUi(){
        navBarTitle.setText(getString(R.string.cash_out));
    }
}
