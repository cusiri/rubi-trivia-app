package com.quiz.calvinusiri.triviaapp.Controllers.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quiz.calvinusiri.triviaapp.Controllers.Activities.LoginMainActivity;
import com.quiz.calvinusiri.triviaapp.Model.Answer;
import com.quiz.calvinusiri.triviaapp.Model.Questions;
import com.quiz.calvinusiri.triviaapp.Model.ServerConfigStrings;
import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.MyBounceInterpolator;
import com.quiz.calvinusiri.triviaapp.Utils.ServerLookUpFunctions;
import com.quiz.calvinusiri.triviaapp.Utils.ServerReceiveFromDB;
import com.quiz.calvinusiri.triviaapp.Utils.ServerSendToDB;

import java.util.Map;

public class ShowAnswersFragmentController extends Fragment {
    private RelativeLayout mainLayout;

    private ImageView imageViewShowAnswer;
    private Dialog extraLifeDialogue;

    private String gameRound;


    private Button option1;
    private Button option2;
    private Button option3;
    private Button extraLifeButton;

    private TextView questionTextView;
    private TextView extraLifeTimer;

    private ProgressBar extraLifeProgressbar;
    private Animation myAnim;

    private Handler checkToSeeIfUserIsSignedInShowAnswersFragement;
    private Handler showAnswersFragmentRecieveCurrentLoggedInUserInfo;
    private Handler showAnswersFragmentResponseToExtraLifeBeingUsedHandler;
    private Handler showAnswersFragmentReceiveGameRoundSoThatTheUserCanGetBackIntoTheGameHandler;
    public static Handler checkWhereUserWasMarkedCorrectOrInCorrectGameMainActivityHandler;
    private Handler receiveQuestionsDBQuestionsFragmentHandler;
    private Handler receiveAnswerFromTheDBTODisplayToUserHandler;

    private static final String TAG = "ShowAnswersFragmentCont";
    private static final String myPreferencesExtraLives = "stored_preferences_extra_lives";
    private static final String hasShownExtraLifeKey = "hasShownExtraLifeKey";

    private ServerConfigStrings serverConfigStrings = new ServerConfigStrings();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_answers,container,false);

        mainLayout = view.findViewById(R.id.fragment_show_answers_main_layout_id);
        imageViewShowAnswer = view.findViewById(R.id.fragment_show_answer_image_view_show_answer_id);

        questionTextView = view.findViewById(R.id.fragment_layout_questions_question_id);
        option1 = view.findViewById(R.id.fragment_layout_questions_first_answer_button_id);
        option2 = view.findViewById(R.id.fragment_layout_questions_second_answer_button_id);
        option3 = view.findViewById(R.id.fragment_layout_questions_third_answer_button_id);

        extraLifeDialogue = new Dialog(getContext());
        myAnim = AnimationUtils.loadAnimation(getContext(), R.anim.bounce); // Button animation for the button

        if (isAdded()){
            handlerCallBacks();
            configureBounceInterpolator();
            ServerReceiveFromDB.receiveCurrentUserMostRecentRoundExperienced(checkWhereUserWasMarkedCorrectOrInCorrectGameMainActivityHandler);
            ServerReceiveFromDB.receiveRoundFromServer(showAnswersFragmentReceiveGameRoundSoThatTheUserCanGetBackIntoTheGameHandler,serverConfigStrings.getGAME_ROUNDS(),"@Rubi_Trivia_Tz",serverConfigStrings.getGAMEROUND()); // So that we can get user back into game
        }

        return view;
    }

    @Override
    public void onStart() {
        ServerLookUpFunctions.serverLookUpIfSignedIn(checkToSeeIfUserIsSignedInShowAnswersFragement);
        super.onStart();
    }

    //Methods

    private void handlerCallBacks(){
        // Handler that checks to see if the user is signed up
        checkToSeeIfUserIsSignedInShowAnswersFragement = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.arg1 == 0){
                    Intent intent = new Intent(getContext(),LoginMainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
                return false;
            }
        });

        // Check to see what the user was marked as
        checkWhereUserWasMarkedCorrectOrInCorrectGameMainActivityHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                // Check to see if the response is -1 or not. -1 means the user got the answer incorrect
                if(message.arg1 == -1){
                    answerIsInCorrectSetLayout();
                    // Check to see if they have an extra life
                    ServerReceiveFromDB.receiveLoggedInUserInformationFromServerOnce(showAnswersFragmentRecieveCurrentLoggedInUserInfo);
                }else{
                    answerIsCorrectSetLayout();
                }
                return false;
            }
        });

        // Receive Current User Logged In Informtion
        showAnswersFragmentRecieveCurrentLoggedInUserInfo = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.obj != null){
                    Map usersMap = (Map) message.obj;
                    Users users = (Users) usersMap.get("user");
                    checkToSeeIfUserHasExtraLife(users);
                }else{
                    Toast.makeText(getContext(),getString(R.string.poor_internet),Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        //Extra Life Used
        showAnswersFragmentResponseToExtraLifeBeingUsedHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                // Launch A pop to show that user is back in the back TODO This potion should be done server side. ??
                // TODO The watch only function must be better. Can't keep showing the error fragment

                return false;
            }
        });

        //Get game rounds From the server
        showAnswersFragmentReceiveGameRoundSoThatTheUserCanGetBackIntoTheGameHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                // Update users information with the current round so that that can stay in the game
                if(message.obj != null){
                    String round = (String) message.obj;
                    gameRound = round;
                    if(!round.isEmpty()){
                        ServerReceiveFromDB.receiveGameQuestionsFromTheDb(receiveQuestionsDBQuestionsFragmentHandler,Integer.parseInt(round), "@Rubi_Trivia_Tz");
                        ServerReceiveFromDB.queryRoundToGetAnswerFromDB(receiveAnswerFromTheDBTODisplayToUserHandler,"@Rubi_Trivia_Tz",round);
                    }
                }else{
                    Toast.makeText(getContext(),getString(R.string.poor_internet),Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        receiveQuestionsDBQuestionsFragmentHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                Questions questions = (Questions) message.obj;
                if(questions != null){
                    setQuestions(questions);
                }else{
                    Toast.makeText(getContext(),getString(R.string.poor_internet),Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        receiveAnswerFromTheDBTODisplayToUserHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                Answer answer = (Answer) message.obj;
                if(answer != null){
                    illuminateCorrectAnswer(option1,option2,option3,answer.getAnswer());
                }else{
                    Toast.makeText(getContext(), getString(R.string.poor_internet), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }

    private void setQuestions(Questions q){
        Activity activity = getActivity();
        if(activity != null && isAdded()){
            if(q!=null){
                questionTextView.setText(q.getQuestion());
                option1.setText(q.getOption1());
                option2.setText(q.getOption2());
                option3.setText(q.getOption3());
            }else{
                //No more questions, game is over for you.
                disableButtons();
            }
            mainLayout.setVisibility(View.VISIBLE);
        }
    }

    private void illuminateCorrectAnswer(final Button o1,final Button o2, final Button o3,final String answer){
        // This is time consuming so load before or, find a way to compare integers
        new Thread(new Runnable() {
            public void run() {
                // a potentially time consuming task
                String option1 = (String) o1.getText();
                String option2 = (String) o2.getText();
                String option3 = (String) o3.getText();

                if(option1.equals(answer)){
                    postThreadUpdateButton(o1);
                }else if(option2.equals(answer)){
                    postThreadUpdateButton(o2);
                }
                else if(option3.equals(answer)){
                    postThreadUpdateButton(o3);
                }else{
                    return; // Dont post anything
                }

            }
        }).start();
    }

    private void postThreadUpdateButton(final Button button){
        if(button != null){
            button.post(new Runnable() {
                public void run() {
                    button.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.correct_green));
                }
            });
        }
    }

    private void disableButtons(){
        if(isAdded()){
            option1.setEnabled(false);
            option2.setEnabled(false);
            option3.setEnabled(false);
        }
    }

    private void answerIsInCorrectSetLayout(){
        imageViewShowAnswer.setImageResource(R.drawable.ic_incorrect_answer_imgae);
    }

    private void answerIsCorrectSetLayout(){
        imageViewShowAnswer.setImageResource(R.drawable.ic_checked);
    }

    private void checkToSeeIfUserHasExtraLife(Users users){
        try{
            Activity activity = getActivity();
            if(isAdded() && activity != null){
                if(users.getel() && !getHasShownExtraLifeSharePreferences(hasShownExtraLifeKey)){
                    // User has extra life == true and user has not seen the extra life pop up yet == true
                    displayExtraLifePopup();
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }

    private void displayExtraLifePopup(){
        setHasShownExtraLivesSharedPreferences(true);
        extraLifeDialogue.setContentView(R.layout.layout_get_extra_life_dialogue_box);
        extraLifeButton = extraLifeDialogue.findViewById(R.id.layout_get_extra_life_button_id);
        extraLifeProgressbar = extraLifeDialogue.findViewById(R.id.layout_get_extra_life_progressBar);
        extraLifeTimer = extraLifeDialogue.findViewById(R.id.layout_get_extra_life_timer_id);

        extraLifeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // If the round changes before you have opted for an extra life then you are out
                //TODO Also need to change the watching only mode because at the moment it shows the wrong answer everytime
                // Get the user back in the game
                updateRoundAndGetUserBackInGame(gameRound);
                ServerSendToDB.genericUpdateBoolean(showAnswersFragmentResponseToExtraLifeBeingUsedHandler,"game_users"); // TODO This should be done server side, removing the extra life from the user
                extraLifeDialogue.dismiss();
            }
        });

        extraLifeDialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        extraLifeDialogue.show();
        initiateTimer();
    }

    private void updateRoundAndGetUserBackInGame(String round){
        ServerSendToDB.sendCurrentUserMostRecentRoundExcerienced(Integer.parseInt(round));
        Toast.makeText(getContext(),"You are back in the game !", Toast.LENGTH_SHORT).show();
    }

    private void initiateTimer(){
        new CountDownTimer(7000, 1000) {
            public void onTick(long millisUntilFinished) {
                Long stringValue = millisUntilFinished / 1000;
                String timer = String.valueOf(stringValue);
                extraLifeTimer.setText(timer);
                extraLifeTimer.startAnimation(myAnim);
                extraLifeProgressbar.setProgress(Integer.parseInt(timer));
            }
            public void onFinish() {
                extraLifeDialogue.dismiss();// Times up, cannot get extra lives anymore
            }
        }.start();
    }

    private void configureBounceInterpolator(){
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

    }

    // Shared Preferences
    private void setHasShownExtraLivesSharedPreferences(Boolean bool){
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(myPreferencesExtraLives, Context.MODE_PRIVATE);
        if(sharedPreferences != null){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(hasShownExtraLifeKey, bool);
            editor.apply();
        }
    }

    private Boolean getHasShownExtraLifeSharePreferences(String key){
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(myPreferencesExtraLives, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key,false);
    }


}
