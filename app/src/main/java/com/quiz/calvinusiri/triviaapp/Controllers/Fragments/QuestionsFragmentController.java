package com.quiz.calvinusiri.triviaapp.Controllers.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quiz.calvinusiri.triviaapp.Controllers.Activities.LoginMainActivity;
import com.quiz.calvinusiri.triviaapp.Model.Questions;
import com.quiz.calvinusiri.triviaapp.Model.ServerConfigStrings;
import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.MyBounceInterpolator;
import com.quiz.calvinusiri.triviaapp.Utils.ServerLookUpFunctions;
import com.quiz.calvinusiri.triviaapp.Utils.ServerReceiveFromDB;

import static com.quiz.calvinusiri.triviaapp.Controllers.Activities.GameMainActivity.checkIfAnswersAreInTheDBReceiveAnswersGameMainActivityHandler;


public class QuestionsFragmentController extends Fragment {
    private RelativeLayout mainLayout;

    private TextView timerTextView;
    private TextView questionTextView;

    private ProgressBar timerProgress;
    private Animation myAnim;

    private Button option1;
    private Button option2;
    private Button option3;

    private Integer currentRoundInteger;

    private Handler receiveQuestionsDBQuestionsFragmentHandler;
    private Handler receiveGameTimerFromDBHandler;
    private Handler checkToSeeIfUserIsSignedInQuestionsFragmentHandler;
    private Handler receiveRoundInformationQuestionsFragmentHandler;

    private String answerBeingSentToTheServerForGradeing="x";
    private static final String TAG = "QuestionsFragementContr";
    private static final String shouldLetUserInGame = "shouldLetUserBackInGame";
    private static final String myPreferencesInGame = "stored_preferences_in_game";

    private boolean hasQuestionsBeenSentToServerForGrading = false;

    private ServerConfigStrings serverConfigStrings = new ServerConfigStrings();


    @Override
    public void onStart() {
        ServerLookUpFunctions.serverLookUpIfSignedIn(checkToSeeIfUserIsSignedInQuestionsFragmentHandler);
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragement_layout_questions,container,false);

        timerTextView =  view.findViewById(R.id.timer_id);
        timerProgress = view.findViewById(R.id.progressBar);
        questionTextView = view.findViewById(R.id.fragment_layout_questions_question_id);

        option1 = view.findViewById(R.id.fragment_layout_questions_first_answer_button_id);
        option2 = view.findViewById(R.id.fragment_layout_questions_second_answer_button_id);
        option3 = view.findViewById(R.id.fragment_layout_questions_third_answer_button_id);

        mainLayout = view.findViewById(R.id.fragment_layout_questions_main_layout_id);

        myAnim = AnimationUtils.loadAnimation(getContext(), R.anim.bounce); // Button animation for the button

        if(isAdded()){
            //Handlers
            handleCallBacks();
            configureBounceInterpolator();
            if(!getShouldLetUserIntoGameSharePreferences(shouldLetUserInGame)){
                // This means the user is not in the game and is in view mode
                userNotInGameViewMode();
            }

            //Get Game information from the server so that we can use the round to pull the question and answers
            ServerReceiveFromDB.receiveRoundFromServer(receiveRoundInformationQuestionsFragmentHandler,serverConfigStrings.getGAME_ROUNDS(),"@Rubi_Trivia_Tz",serverConfigStrings.getGAMEROUND()); // So that we can get user game into game
            setOnClickListeners();
        }

        return view;
    }

    private void handleCallBacks() {
        // Handler that checks to see if the user is signed up
        checkToSeeIfUserIsSignedInQuestionsFragmentHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                try{
                    if(message.arg1 == 0){
                        Intent intent = new Intent(getContext(),LoginMainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                }catch (Exception e){e.printStackTrace();}
                return false;
            }
        });

        receiveQuestionsDBQuestionsFragmentHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                Questions questions = (Questions) message.obj;
                setQuestions(questions);
                return false;
            }
        });

        receiveGameTimerFromDBHandler = new Handler(new Handler.Callback() { // This can go, ALREADY HAVE GAME TIMER
            @Override
            public boolean handleMessage(Message message) {
                Long gameTimer = (Long) message.obj;
                String gameTimerString = String.valueOf(gameTimer);

                timerTextView.setText(gameTimerString);
                timerTextView.startAnimation(myAnim);
                timerProgress.setProgress(Integer.parseInt(gameTimerString));

                if(gameTimer == 0){
                    // Game is over
                    setQuestions(null); // No questions, users can be set as out right here
                    if(!hasQuestionsBeenSentToServerForGrading && isAdded()){
                        setAnswer(answerBeingSentToTheServerForGradeing);
                    }
                }
                return false;
            }
        });

        receiveRoundInformationQuestionsFragmentHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                String round = (String) message.obj;
                currentRoundInteger = Integer.parseInt(round);
                if(!round.isEmpty()){
                    ServerReceiveFromDB.receiveGameQuestionsFromTheDb(receiveQuestionsDBQuestionsFragmentHandler,currentRoundInteger, "@Rubi_Trivia_Tz");
                    ServerReceiveFromDB.receiveGameTimer(receiveGameTimerFromDBHandler,"@Rubi_Trivia_Tz");
                }else {
                    Toast.makeText(getContext(),getString(R.string.poor_internet),Toast.LENGTH_LONG).show();
                }

                return false;
            }
        });
    }

    private void setQuestions(Questions q){
        Activity activity = getActivity();
        if(activity != null && isAdded()){
            if(getShouldLetUserIntoGameSharePreferences(shouldLetUserInGame)){
                enableButtons(); // Means the user is in the game
            }
            if(q!=null){
                questionTextView.setText(q.getQuestion());
                option1.setText(q.getOption1());
                option2.setText(q.getOption2());
                option3.setText(q.getOption3());
            }else{
                //No more questions, game is over for you.
                disableButtons();
                questionTextView.setText(getText(R.string.game_over_times_up));
                questionTextView.startAnimation(myAnim);
            }
            mainLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setAnswer(String answerUserd){
        ServerLookUpFunctions.serverLookUpIfTheAnswerIsInTheDataBase(answerUserd,"@Rubi_Trivia_Tz",currentRoundInteger,checkIfAnswersAreInTheDBReceiveAnswersGameMainActivityHandler);
    }

    private void setOnClickListeners(){
        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAdded()){
                    answerBeingSentToTheServerForGradeing = option1.getText().toString().trim();
                    option1.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                    option1.setEnabled(false);
                    hasQuestionsBeenSentToServerForGrading = true;
                    setAnswer(answerBeingSentToTheServerForGradeing);
                    disableButtons();
                }

                return;
            }
        });
        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAdded()){
                    answerBeingSentToTheServerForGradeing = option2.getText().toString().trim();
                    option2.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                    option2.setEnabled(false);
                    hasQuestionsBeenSentToServerForGrading = true;
                    setAnswer(answerBeingSentToTheServerForGradeing);
                    disableButtons();
                }

                return;
            }
        });
        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAdded()){
                    answerBeingSentToTheServerForGradeing = option3.getText().toString().trim();
                    option3.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                    hasQuestionsBeenSentToServerForGrading = true;
                    setAnswer(answerBeingSentToTheServerForGradeing);
                    disableButtons();
                }
                return;
            }
        });
    }

    private void userNotInGameViewMode(){
        disableButtons();
        Toast.makeText(getContext(), getString(R.string.eliminated),Toast.LENGTH_SHORT).show();
    }

    private void enableButtons(){
        if(isAdded()){
            option1.setEnabled(true);
            option2.setEnabled(true);
            option3.setEnabled(true);
        }
    }

    private void disableButtons(){
        if(isAdded()){
            option1.setEnabled(false);
            option2.setEnabled(false);
            option3.setEnabled(false);
        }
    }

    private void configureBounceInterpolator(){
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

    }

    //Shared prefereces
    private Boolean getShouldLetUserIntoGameSharePreferences(String key){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(myPreferencesInGame, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key,false);
    }
}
