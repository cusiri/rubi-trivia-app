package com.quiz.calvinusiri.triviaapp.Utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.quiz.calvinusiri.triviaapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class ShowWinnersRecyclerViewAdapter extends FirestoreRecyclerAdapter<Users, ShowWinnersRecyclerViewAdapter.ViewHolder> {
    private Integer individualWinnings;
    public ShowWinnersRecyclerViewAdapter(@NonNull FirestoreRecyclerOptions options, Integer inWinnings) {
        super(options);
        this.individualWinnings = inWinnings;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Users model) {
        holder.userTextView.setText(String.valueOf(model.setEmailAsReference(model.getE())));
        holder.userWinnings.setText(String.valueOf(individualWinnings));
//        holder.imageview.setImageResource(R.drawable.ic_person_outline_black_24dp);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_profile_snippet,parent,false);
        return new ViewHolder(view);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageview;

        private TextView userTextView;
        private TextView userWinnings;

        private RelativeLayout parentLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            userTextView = itemView.findViewById(R.id.layout_profile_snippet_text_view_id);
            userWinnings = itemView.findViewById(R.id.layout_profile_snippet_winnings_text_view_id);

            imageview = itemView.findViewById(R.id.layout_profile_snippet_image_view_id);
            parentLayout = itemView.findViewById(R.id.layout_profile_snippet_parent_layout_id);
        }
    }
}
