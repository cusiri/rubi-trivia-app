package com.quiz.calvinusiri.triviaapp.Controllers.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.quiz.calvinusiri.triviaapp.R;

public class SubmitTriviaMainActivity extends AppCompatActivity {
    private ImageView backButton;
    private TextView navBarHeader;

    private Context mContext = SubmitTriviaMainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_trivia_main);

        backButton = findViewById(R.id.snipet_nav_bar_back_button_id);
        navBarHeader = findViewById(R.id.snipet_nav_bar_title_id);

        setUpView();
        onClickListeners();
    }

    private void onClickListeners(){
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MainMenuMainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    private void setUpView(){
        navBarHeader.setText(getString(R.string.submit_trivia));
    }
}
