package com.quiz.calvinusiri.triviaapp.Controllers.Activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.SliderAdpater;

public class InstructionsMainActivity extends AppCompatActivity {
    private ViewPager viewPager;

    private Button nextButton;
    private Button previousButton;

    private SliderAdpater sliderAdpater;

    protected Integer mCurrentPage;

    private Boolean atEndOfSlider = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_instructions_main);
        viewPager = findViewById(R.id.activity_instructions_main_layout_view_pager_id);

        nextButton = findViewById(R.id.activity_instructions_main_next_button_id);
        previousButton = findViewById(R.id.activity_instructions_main_back_button_id);

        sliderAdpater = new SliderAdpater(InstructionsMainActivity.this);
        viewPager.setAdapter(sliderAdpater);

        setUpListeners();

    }

    private void setUpListeners(){
        mCurrentPage = 0; // Initiate the current page
        if(viewPager != null){
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    mCurrentPage = position;
                    try{
                        if(position == 0){
                            // first page, shouldn't be able to go back
                            previousButton.setEnabled(false);
                            previousButton.setVisibility(View.INVISIBLE);
                            atEndOfSlider = false;
                        }
                        else if(position == sliderAdpater.slider_headings.length - 1){
                            previousButton.setEnabled(true);
                            previousButton.setVisibility(View.VISIBLE);

                            nextButton.setEnabled(true);
                            nextButton.setVisibility(View.VISIBLE);

                            nextButton.setText("Finish");

                            atEndOfSlider = true;
                        }else{
                            nextButton.setEnabled(true);
                            previousButton.setEnabled(true);

                            previousButton.setVisibility(View.VISIBLE);
                            nextButton.setText("Next");

                            atEndOfSlider = false;

                        }
                    }catch (Exception e){e.printStackTrace();}

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                        if(atEndOfSlider){
                            Intent intent = new Intent(InstructionsMainActivity.this,MainMenuMainActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            viewPager.setCurrentItem(mCurrentPage + 1);
                        }
                    }catch (Exception e){e.printStackTrace();}

                }
            });

            previousButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(viewPager != null){
                        viewPager.setCurrentItem(mCurrentPage - 1);
                    }
                }
            });
        }
    }
}
