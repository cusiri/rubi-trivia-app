package com.quiz.calvinusiri.triviaapp.Utils;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;

import com.quiz.calvinusiri.triviaapp.Model.ServerConfigStrings;
import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;

import static com.quiz.calvinusiri.triviaapp.Controllers.Activities.SignUpMainActivity.sendUserInformationToDBIfSignUpWorksHandler;

public class ServerSendToDB {
    private static FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private static FirebaseDatabase database = FirebaseDatabase.getInstance();
    private static FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    private static final String TAG = "ServerSendToDB";
    private static ServerConfigStrings serverConfigStrings = new ServerConfigStrings();

    // SEND TO SERVER
    public static void sendNewUserToDBOnSignUp(Object object, final Handler handler){
        if(mAuth.getCurrentUser()!=null){
            Users users = (Users) object;
            firestore.collection(serverConfigStrings.getGAME_USERS()).document(mAuth.getCurrentUser().getUid()).set(users).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    sendMessageWithArgAlone(1, handler);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    sendMessageWithArgAlone(0, handler);
                }
            });
        }
    }

    public static void sendCurrentUserMostRecentRoundExcerienced(Integer round){
        if(mAuth.getCurrentUser()!=null){
            firestore.collection(serverConfigStrings.getGAME_USERS()).document(mAuth.getCurrentUser().getUid()).update(serverConfigStrings.getR(),round).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            });
        }
    }

    //Firestore
    public static void genericUpdateBoolean(final Handler handler,String parent){
        if(mAuth != null){
            firestore.collection(parent).document(mAuth.getCurrentUser().getUid()).update(serverConfigStrings.getEL(),false)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            sendMessageWithArgAlone(1,handler);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            sendMessageWithArgAlone(0,handler);
                        }
                    });
        }
    }

    public static void updateUserAwards(final Handler handler,String parent,Integer amount){
        if(mAuth != null){
            firestore.collection(parent).document(mAuth.getCurrentUser().getUid()).update(serverConfigStrings.getAW(),amount)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            sendMessageWithArgAlone(1,handler);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            sendMessageWithArgAlone(0,handler);
                        }
                    });
        }
    }

    public static void sendUpdateUserLogOut(){
        FirebaseAuth.getInstance().signOut();
    }

    // Utils
    private static void sendMessageWithArgAlone(int arg, Handler handler){
        try{
            Message message = handler.obtainMessage();
            message.arg1 = arg;
            handler.sendMessage(message);
        }catch (Exception e){e.printStackTrace();}

    }
}
