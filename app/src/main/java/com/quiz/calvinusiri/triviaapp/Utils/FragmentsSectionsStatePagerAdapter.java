package com.quiz.calvinusiri.triviaapp.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class FragmentsSectionsStatePagerAdapter extends FragmentStatePagerAdapter{
    //Keeping track of fragments
    private final List<Fragment> mFragmentList = new ArrayList<>();
    public FragmentsSectionsStatePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    // Used to add fragments to adapter
    public void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    public void clear(){
        mFragmentList.clear();
    }
}
