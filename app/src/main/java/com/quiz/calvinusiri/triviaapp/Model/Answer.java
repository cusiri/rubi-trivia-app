package com.quiz.calvinusiri.triviaapp.Model;

public class Answer {
    String answer;
    String round;

    public Answer() {
    }

    public Answer(String answer, String round) {
        this.answer = answer;
        this.round = round;
    }

    public String getAnswer() {
        return answer;
    }

    public String getRound() {
        return round;
    }
}
