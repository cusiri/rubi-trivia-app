package com.quiz.calvinusiri.triviaapp.Controllers.Activities;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;


import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.LeaderboardRecyclerViewAdapter;
import com.quiz.calvinusiri.triviaapp.Utils.ServerLookUpFunctions;
import com.quiz.calvinusiri.triviaapp.Utils.ServerReceiveFromDB;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.Query;

public class LeaderboardMainActivity extends AppCompatActivity {
    private ImageView backButton;
    private LeaderboardRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private Handler leaderboardMainActivityCheckIfUserIsLoggedOnHandler;
    private Handler leaderboardMainActivitySortedUserForLeaderboardHandler;
    private static final String TAG = "LeaderboardMainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard_main);
        recyclerView = findViewById(R.id.activity_leaderboard_main_layoutrecycler_view);
        backButton = findViewById(R.id.snipet_nav_bar_back_button_id);
        leaderboardMainActivityCheckIfUserIsLoggedOnHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.arg1 == 0){
                    Intent intent = new Intent(LeaderboardMainActivity.this,LoginMainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    ServerReceiveFromDB.queryAndOrderUsersFromDb(leaderboardMainActivitySortedUserForLeaderboardHandler);
                }
                return false;
            }
        });
        leaderboardMainActivitySortedUserForLeaderboardHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(final Message message) {
                Query query = (Query) message.obj;
                FirestoreRecyclerOptions<Users> options = new FirestoreRecyclerOptions.Builder<Users>()
                    .setQuery(query,Users.class)
                    .build();
                adapter = new LeaderboardRecyclerViewAdapter(options);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(LeaderboardMainActivity.this));
                recyclerView.setAdapter(adapter);
                adapter.startListening();
                return false;
            }
        });
        // Onclicklistener
        setOnClickListeners();
    }
    private void setOnClickListeners(){
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeaderboardMainActivity.this,MainMenuMainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        ServerLookUpFunctions.serverLookUpIfSignedIn(leaderboardMainActivityCheckIfUserIsLoggedOnHandler);
        if (adapter != null) {
            adapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null) {
            adapter.stopListening();
        }
    }
}
