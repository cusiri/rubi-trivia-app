package com.quiz.calvinusiri.triviaapp.Utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quiz.calvinusiri.triviaapp.R;

public class SliderAdpater extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;

    public SliderAdpater(Context context) {
        this.context = context;
    }

    //Image Assets For the sliding view
    public int [] slider_images = {
            R.drawable.notifications,
            R.drawable.notifications,
            R.drawable.notifications,
            R.drawable.out,
            R.drawable.winner,
            R.drawable.roll_over,
            R.drawable.cash_out,
            R.drawable.still_have_qustions
    };

    //String Array for headings
    public String [] slider_headings = {
            "Welcome to Rubi",
            "Join the live game",
            "Answer questions, think fast",
            "Miss a question, you are out",
            "Get all the questions right in time and you win",
            "No winners, Hakuna Matata",
            "Pay Me",
            "Still have questions?"
    };

    //String array for descriptions
    public String[] slider_descriptions = {
            "Lets walk through how to play the game!",
            "We'll send you a push notification. \n Dont be late",
            "Pick the correct answer\n You have 10 seconds for each question",
            "If you tap the incorrect answer or run out of time, you are eliminated. \n You can still watch the game",
            "If you make it to the end, you win. If more than one person wins, the prize is split",
            "If everyone is eliminated, the prize will roll over to the next game",
            "Want your money?\n Cash out right from the app!",
            "Find us on \n \n Twitter @Rubitrivia \n \n Facebook Rubitrivia"
    };

    @Override
    public int getCount() {
        return slider_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        // Making the view the relative layout
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_instructions_sliding_layout,container,false);

        ImageView slidingImageView = view.findViewById(R.id.layout_instructions_sliding_layout_image_id);
        TextView slidingHeader = view.findViewById(R.id.layout_instructions_sliding_layout_heading_id);
        TextView slidingDescription = view.findViewById(R.id.layout_instructions_sliding_layout_description_id);

        if(position <= slider_headings.length -1){
            slidingHeader.setText(slider_headings[position]);
        }
        if(position <= slider_images.length -1){
            slidingImageView.setImageResource(slider_images[position]);
        }
        if(position <= slider_descriptions.length -1){
            slidingDescription.setText(slider_descriptions[position]);
        }

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}
