package com.quiz.calvinusiri.triviaapp.Controllers.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.HideKeyboard;
import com.quiz.calvinusiri.triviaapp.Utils.ServerLookUpFunctions;
import com.quiz.calvinusiri.triviaapp.Utils.ServerSendToDB;

import java.util.ArrayList;
import java.util.List;

public class SignUpMainActivity extends AppCompatActivity {
    private EditText fullname;
    private EditText email;
    private EditText password;
    private EditText confirmPassword;
    private EditText referalCode;

    private Button signUp;

    private Handler checkIfUserWasSignedUpCorrectlyHandler;
    public static Handler sendUserInformationToDBIfSignUpWorksHandler;

    private ProgressDialog mProgress;

    private RelativeLayout mainLayout;

    private TextView alreadyHaveAnAccountTextView;
    private Context mContext = SignUpMainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_main);

        email = findViewById(R.id.sign_up_edit_text_email);
        password = findViewById(R.id.sign_up_edit_text_password);
        confirmPassword = findViewById(R.id.sign_up_edit_text_confirm_password);
        signUp = findViewById(R.id.sign_up_button_signup);
        referalCode = findViewById(R.id.sign_up_edit_text_referal_code_id);

        mProgress = new ProgressDialog(this);

        alreadyHaveAnAccountTextView = findViewById(R.id.activity_sign_up_main_already_have_an_account_id);
        mainLayout = findViewById(R.id.activity_signup_main_background_layout_id);
        //Handlers

        handleCallBacks();
        setUpOnClickListeners();

        setUpProgressDialog();
        setUpFullScreenView();

        hideKeyboard();
    }

    private void setUpOnClickListeners(){
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Try Signup user
                if(confirmPassword.getText().toString().trim().equals(password.getText().toString().trim())){
                    if(!email.getText().toString().trim().isEmpty() && !password.getText().toString().trim().isEmpty()) {
                        mProgress.show();
                        ServerLookUpFunctions.serverLookUpIfSignUpSuccessful(email.getText().toString().trim(),password.getText().toString().trim(),checkIfUserWasSignedUpCorrectlyHandler);
                    }else{
                        Toast.makeText(mContext,String.valueOf(getText(R.string.empty_fields)),Toast.LENGTH_LONG).show();
                    }

                }else{
                    Toast.makeText(mContext,"Password must match",Toast.LENGTH_LONG).show();
                }
            }
        });
        alreadyHaveAnAccountTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateActivities(mContext, LoginMainActivity.class);
            }
        });
    }

    private void handleCallBacks(){
        checkIfUserWasSignedUpCorrectlyHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.arg1 == 1){
                    // Signup was successful, send information to the database
                    List<String> subscribers = new ArrayList();
                    String refCode = referalCode.getText().toString().trim();
                    if(TextUtils.isEmpty(refCode)){
                        refCode = "";
                    }
                    subscribers.add("subscribers");

                    Users users = new Users(email.getText().toString().trim(),
                            Long.parseLong("0"),
                            refCode,false,
                            -1,subscribers);

                    ServerSendToDB.sendNewUserToDBOnSignUp(users,sendUserInformationToDBIfSignUpWorksHandler);
                    /*
                     * The game that you subscribe to will eventually be the game that is in your location - country.
                     * */
                }else{
                    // Signup was unsuccessful tell user to check internet connection
                    Toast.makeText(SignUpMainActivity.this,"Check Internet Connection, Password should also be greater than 5 characters",Toast.LENGTH_LONG).show();
                    mProgress.dismiss();
                }
                return false;
            }
        });

        sendUserInformationToDBIfSignUpWorksHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                Toast.makeText(mContext,"Welcome To The Game",Toast.LENGTH_LONG).show();
                navigateActivities(mContext,InstructionsMainActivity.class);
                mProgress.dismiss();
                return false;
            }
        });
    }

    private void setUpProgressDialog(){
        mProgress.setTitle("Processing...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
    }

    private void setUpFullScreenView(){
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void hideKeyboard(){
        HideKeyboard keyboard = new HideKeyboard(SignUpMainActivity.this);
        keyboard.setupUI(mainLayout);
    }

    private void navigateActivities(Context currentContext, Class movingClass){
        if(currentContext != null){
            Intent intent = new Intent(currentContext,movingClass);
            startActivity(intent);
            finish();
        }
    }
}
