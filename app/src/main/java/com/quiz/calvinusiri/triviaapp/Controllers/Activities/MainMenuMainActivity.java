package com.quiz.calvinusiri.triviaapp.Controllers.Activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quiz.calvinusiri.triviaapp.Model.GameInformation;
import com.quiz.calvinusiri.triviaapp.Model.ServerConfigStrings;
import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.MyBounceInterpolator;
import com.quiz.calvinusiri.triviaapp.Utils.ServerLookUpFunctions;
import com.quiz.calvinusiri.triviaapp.Utils.ServerReceiveFromDB;
import com.quiz.calvinusiri.triviaapp.Utils.ServerSendToDB;

import java.util.HashMap;
import java.util.Map;

public class MainMenuMainActivity extends AppCompatActivity {
    private TextView userFullName;
    private TextView userAmountWonSoFar;
    private TextView nextGamePrizeMoney;
    private TextView nextGameStatusInformationTextView;
    private TextView nextGameDate;
    private TextView leaderboardButton;
    private TextView inviteUsersButton;

    private Button letsPlayButton;
    private Button helpMenuButton;
    private Button extraLifeButton;


    private String gameDate;
    private String gameTime;
    private String gamePrize;
    private String userUid = "";

    private Integer gameStatus;

    private Handler receiveUserInformationMainMenuFragmentHandler;
    private Handler checkToSeeIfUserIsSignedInMainMenuActivityHandler;
    private Handler receiveGameStatusForMainMenuActivityHandler;

    private LinearLayout background;
    private LinearLayout balanceLinearLayout;

    private AnimationDrawable animationDrawable;
    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;

    private Animation myAnim;

    private Context mContext = MainMenuMainActivity.this;
    private static ServerConfigStrings serverConfigStrings = new ServerConfigStrings();

    @Override
    protected void onStart() {
        super.onStart();
        ServerLookUpFunctions.serverLookUpIfSignedIn(checkToSeeIfUserIsSignedInMainMenuActivityHandler);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_menu_main);

        userFullName = findViewById(R.id.fragment_main_menu_full_name_id);
        userAmountWonSoFar = findViewById(R.id.fragment_main_menu_amount_won_id);
        nextGamePrizeMoney = findViewById(R.id.fragment_main_menu_prize_game_text_view_id);
        nextGameStatusInformationTextView = findViewById(R.id.next_game_text_view_id);
        nextGameDate = findViewById(R.id.fragment_main_menu_date_text_view_id);

        leaderboardButton = findViewById(R.id.fragment_main_menu_leaderboard_id);
        inviteUsersButton = findViewById(R.id.fragment_main_menu_invite_id);
        helpMenuButton = findViewById(R.id.activity_main_menu_help_menu_id);
        letsPlayButton = findViewById(R.id.main_menu_lets_play_button_id);
        extraLifeButton = findViewById(R.id.activity_main_menu_extra_life_button_id);

        background = findViewById(R.id.activity_main_menu_layout_id);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        animationDrawable = (AnimationDrawable) background.getBackground();
        navigationView = findViewById(R.id.nav_view);
        balanceLinearLayout = findViewById(R.id.activity_main_menu_linear_layout_balance_id);

        myAnim = AnimationUtils.loadAnimation(mContext, R.anim.bounce); // Button animation for the button



        setOnClickListeners();
        setUpDrawerNavigator();
        setUpBackgroundAnimationScreen();

        callBackHandlers();
        ServerReceiveFromDB.receiveLoggedInUserInformationFromServer(receiveUserInformationMainMenuFragmentHandler);
    }

    //Methods
    private void callBackHandlers(){
        checkToSeeIfUserIsSignedInMainMenuActivityHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.arg1 == 0){
                    doLogOut();
                }else{
                    ServerReceiveFromDB.receiveGameInformationFromServer(receiveGameStatusForMainMenuActivityHandler,"@Rubi_Trivia_Tz");
                }
                return false;
            }
        });

        // Receive game status information so that it will effect what is show on the screen PUT HERE....
        receiveGameStatusForMainMenuActivityHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                GameInformation gameInformation = (GameInformation) message.obj;
                if(gameInformation != null){
                    setUpGameInformation(gameInformation);
                }else{
                    Toast.makeText(mContext, String.valueOf(getText(R.string.poor_internet)),Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        //Receive user information from the db
        receiveUserInformationMainMenuFragmentHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                Map userMap = (HashMap) message.obj;
                if(userMap != null){

                    Users usersInformation = (Users) userMap.get(serverConfigStrings.getUSER());
                    userUid = (String) userMap.get(serverConfigStrings.getUID());

                    if(usersInformation != null) {
                        setUpUserInformation(usersInformation);
                    }
                }else{
                    Toast.makeText(mContext, String.valueOf(getText(R.string.poor_internet)),Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }


    private void setOnClickListeners(){
        try{
            letsPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(gameStatus != null){
                        if(gameStatus == 0){
                            Toast.makeText(mContext,getString(R.string.check_for_game_time),Toast.LENGTH_SHORT).show();
                        }else{
                            navigateActivities(mContext,GameMainActivity.class);
                        }
                    }else{
                        Toast.makeText(mContext,getString(R.string.poor_internet),Toast.LENGTH_SHORT).show();
                    }
                }
            });
            leaderboardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    navigateActivities(mContext,LeaderboardMainActivity.class);
                }
            });
            inviteUsersButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    inviteUserToPlatform();
                }
            });
            helpMenuButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDrawerLayout.openDrawer(Gravity.START);
                }
            });

            balanceLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    navigateActivities(mContext,CashOutMainActivity.class);
                }
            });

            extraLifeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }catch (Exception e){e.printStackTrace();}

    }

    // Set up views
    private void setUpBackgroundAnimationScreen(){
        if(animationDrawable != null){
            animationDrawable.setEnterFadeDuration(8000);
            animationDrawable.setExitFadeDuration(8000);
            animationDrawable.start();
        }
    }

    private void setUpGameInformation(GameInformation gameInformation){
        try{
            gameStatus = Integer.parseInt(gameInformation.getGameStatus());
            gamePrize = gameInformation.getGamePrize();
            gameTime = gameInformation.getGameTime();
            gameDate = gameInformation.getGameDate();

            if(gameStatus != 0){
                letsPlayButton.setBackgroundResource(R.drawable.layout_pink_button_shape);
                letsPlayButton.setEnabled(true);
                nextGameStatusInformationTextView.setText(R.string.game_is_live);
                configureBounceInterpolator();
                letsPlayButton.startAnimation(myAnim);
            }else{
                // Reset game here when the game status is 0
                letsPlayButton.setBackgroundResource(R.drawable.layout_grey_button_circular);
                nextGameStatusInformationTextView.setText(R.string.next_game_in);

                ServerSendToDB.sendCurrentUserMostRecentRoundExcerienced(-1); // Just to make sure that when the status is 0 the user is reset.
            }

            nextGamePrizeMoney.setText("Prize " + gamePrize + " tsh");
            nextGameDate.setText(gameTime+" "+gameDate);
        }catch (Exception e){e.printStackTrace();}
    }

    private void setUpUserInformation(Users usersInformation){
        try{
            userFullName.setText(usersInformation.setEmailAsReference(usersInformation.getE()));
            userAmountWonSoFar.setText("Tsh " + usersInformation.getAw());

            if(usersInformation.getel()){
                extraLifeButton.startAnimation(myAnim);
                extraLifeButton.setBackgroundResource(R.drawable.ic_have_extra_life);
            }else{
                extraLifeButton.setBackgroundResource(R.drawable.ic_no_have_extra_life);
            }

        }catch (Exception e){e.printStackTrace();}
    }


    private void setUpDrawerNavigator(){
        try{
            navigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem menuItem) {
                            // close drawer when item is tapped
                            mDrawerLayout.closeDrawers();
                            // Add code here to update the UI based on the item selected
                            // For ioAfrica, swap UI fragments here
                            switch (menuItem.getItemId()){
                                case R.id.drawer_view_how_to_play_id:
                                    navigateActivities(mContext,InstructionsMainActivity.class);
                                    break;
//                                case R.id.drawer_view_submit_trivia_id:
//                                    navigateActivities(mContext,SubmitTriviaMainActivity.class);
//                                    break;
                                case R.id.drawer_view_log_out_id:
                                    doLogOut();
                                    break;
                            }
                            return true;
                        }
                    });

        }catch (Exception e){e.printStackTrace();}
    }

    // Util Methods
    private void configureBounceInterpolator(){
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

    }

    private void doLogOut(){
        ServerSendToDB.sendUpdateUserLogOut();
        navigateActivities(mContext,LoginMainActivity.class);
    }

    private void navigateActivities(Context currentContext, Class movingClass){
        if(currentContext != null){
            Intent intent = new Intent(currentContext,movingClass);
            startActivity(intent);
            finish();
        }
    }

    private void inviteUserToPlatform(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        if(!userUid.isEmpty()){
            sendIntent.putExtra(Intent.EXTRA_TEXT, getText(R.string.invite_users_msg)+"\n\n"+userUid);
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.app_name)));
        }else{
            Toast.makeText(mContext, String.valueOf(getText(R.string.poor_internet)),Toast.LENGTH_SHORT).show();
        }
    }
}
