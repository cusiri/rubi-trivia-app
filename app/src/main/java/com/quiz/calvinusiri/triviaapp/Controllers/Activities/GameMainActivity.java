package com.quiz.calvinusiri.triviaapp.Controllers.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.source.hls.BuildConfig;
import com.google.android.exoplayer2.util.Util;
import com.quiz.calvinusiri.triviaapp.Controllers.Fragments.BlankFragmentForViewingPurposes;
import com.quiz.calvinusiri.triviaapp.Controllers.Fragments.QuestionsFragmentController;
import com.quiz.calvinusiri.triviaapp.Controllers.Fragments.ShowAnswersFragmentController;
import com.quiz.calvinusiri.triviaapp.Controllers.Fragments.ShowWinnersFragmentController;
import com.quiz.calvinusiri.triviaapp.Model.GameInformation;
import com.quiz.calvinusiri.triviaapp.Model.ServerConfigStrings;
import com.quiz.calvinusiri.triviaapp.R;

import com.quiz.calvinusiri.triviaapp.Utils.FragmentsSectionsStatePagerAdapter;
import com.quiz.calvinusiri.triviaapp.Utils.ServerLookUpFunctions;
import com.quiz.calvinusiri.triviaapp.Utils.ServerReceiveFromDB;
import com.quiz.calvinusiri.triviaapp.Utils.ServerSendToDB;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;

import java.net.CookieManager;
import java.net.CookiePolicy;

import static com.google.android.exoplayer2.DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
import static com.google.android.exoplayer2.DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON;
import static com.google.android.exoplayer2.DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER;

public class GameMainActivity extends AppCompatActivity{
    private String gameRound;
    private String gameServer;
    protected String userAgent;
    public static final String PREFER_EXTENSION_DECODERS = "prefer_extension_decoders";
    private static final String TAG = "GameMainActivity";
    private static final String myPreferencesExtraLives = "stored_preferences_extra_lives";
    private static final String myPreferencesInGame = "stored_preferences_in_game";
    private static final String hasShownExtraLifeKey = "hasShownExtraLifeKey";
    private static final String shouldLetUserInGame = "shouldLetUserBackInGame";

    private Integer gameStatus;
    private Integer previousStateLookUpForSyncPurposes = 0;

    private ViewPager mViewPager;
    private ProgressBar videoProgressBar;

    private TextView numberOfPlayersStillInGame;
    private TextView logo;

    // Player Variables
    private PlayerView playerView;
    private SimpleExoPlayer player;

    private final BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

    private DefaultTrackSelector trackSelector;

    private static final CookieManager DEFAULT_COOKIE_MANAGER;

    public FragmentStatePagerAdapter adapter = null;

    private Handler checkTheGameStatusMainActivityHandler;
    public static Handler checkIfAnswersAreInTheDBReceiveAnswersGameMainActivityHandler;
    private Handler checkToSeeIfUserIsSignedInGameMainActivityHandler;
    private Handler gameMainActivityCurrentUserMostRecentRoundExpereincedHandler;
    private Handler receiveRoundFromServerHandler;
    private Handler receiveServerInfoHandler;
    private Handler receiveNumberOfPlayersThatAreStillInGameMainActivityHandler;

    private static ServerConfigStrings serverConfigStrings = new ServerConfigStrings();
    private Context mContext = GameMainActivity.this;

    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }


    @Override
    public void onStart() {
        super.onStart();
        ServerLookUpFunctions.serverLookUpIfSignedIn(checkToSeeIfUserIsSignedInGameMainActivityHandler);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Bundle bundle = new Bundle();
        super.onSaveInstanceState(bundle); // Was getting errors, dont care about saved state but needed
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // Keep the screen on during game
        mViewPager = findViewById(R.id.main_view_pager_id);
        playerView = findViewById(R.id.videoView);

        logo = findViewById(R.id.activity_game_main_logo_id);
        numberOfPlayersStillInGame = findViewById(R.id.activity_game_text_view_number_of_players_id);
        videoProgressBar = findViewById(R.id.activity_game_main_blank_vide_progress_bar_id);

        userAgent = Util.getUserAgent(this, "ioAfrica");

        initializePlayer();
        setUpPlayerFullScreenView();
        handleCallBacks();
        setUpOnClickListerners();

        // Reseting, has show extra life to false, always make sure that you come back here so that this resets
        genericBooleanSetSharePreferences(myPreferencesExtraLives,hasShownExtraLifeKey,false);
        ServerReceiveFromDB.receivePlayersStillInGameFromDB(receiveNumberOfPlayersThatAreStillInGameMainActivityHandler);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }


    // Methods
    private void navigationFunction(Integer gameStatus){
        switch (gameStatus){
            case 0:
                // Game has ended, launch the main menu
                releasePlayer();
                resetSharedPreferencesForNextGame();
                navigateActivities(mContext,MainMenuMainActivity.class);
                break;
            case 1:
                //launch advertisment, game is about to start, make sure to update the roundThatTheGameIsCurrentlyOn from the control room
                resetSharedPreferencesForNextGame();
                exchangeFragmentsDuringTransition(new BlankFragmentForViewingPurposes());
                ServerReceiveFromDB.genericReceiveString(receiveServerInfoHandler,serverConfigStrings.getGAME_SERVERS(),serverConfigStrings.getSERVER(),"@Rubi_Trivia_Tz");
                ServerSendToDB.sendCurrentUserMostRecentRoundExcerienced(0); // Just to make sure that when the status is 0 the user is reset.
                break;
            case 3:
                /*Game is live*/
                ServerReceiveFromDB.receiveRoundFromServer(receiveRoundFromServerHandler,serverConfigStrings.getGAME_ROUNDS(),"@Rubi_Trivia_Tz",serverConfigStrings.getGAMEROUND());
                break;
            case 4:
                // Show, video stating the question is being graded. Presenter talks about the question. CLEAR SCREEN.
                exchangeFragmentsDuringTransition(new BlankFragmentForViewingPurposes());
                break;
            case 5:
                // Show answers on the app, show whether the player got it correct of was wrong with some animations
                exchangeFragmentsDuringTransition(new ShowAnswersFragmentController());
                break;
            case 6:
                // General blank screen, remove all fragment Show winners..
                exchangeFragmentsDuringTransition(new ShowWinnersFragmentController());
                break;
            default:
                // Not too sure what to do here so figure it out later
                Toast.makeText(GameMainActivity.this,getText(R.string.poor_internet), Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void handleCallBacks(){

        receiveServerInfoHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                gameServer = (String) message.obj;
                if(!gameServer.isEmpty()){
                    getAndSetUriInPlayer(Uri.parse(gameServer));
                }else{
                    Toast.makeText(GameMainActivity.this,getText(R.string.poor_internet), Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

        checkTheGameStatusMainActivityHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                GameInformation gameInformation = (GameInformation) message.obj;
                if(gameInformation != null){
                    gameStatus = Integer.parseInt(gameInformation.getGameStatus());
                    // The main menu should always launch first
                    if(gameStatus == 0){
                        previousStateLookUpForSyncPurposes = gameStatus;
                        navigationFunction(gameStatus);
                    }else{
                        // If they are late then do this
                        if(gameStatus - 1 != previousStateLookUpForSyncPurposes){
                            navigationFunction(gameStatus);
                            previousStateLookUpForSyncPurposes = gameStatus;
                        }else{
                            navigationFunction(gameStatus);
                            previousStateLookUpForSyncPurposes = gameStatus;
                        }
                    }
                }else{
                    Toast.makeText(GameMainActivity.this,getText(R.string.poor_internet), Toast.LENGTH_LONG).show();
                }

                return false;
            }
        });

        // Handler that checks to see if the user is signed up
        checkToSeeIfUserIsSignedInGameMainActivityHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.arg1 == 0){
                    navigateActivities(mContext,LoginMainActivity.class);
                }else{
                    if(!isFinishing()){
                        ServerReceiveFromDB.receiveGameInformationFromServer(checkTheGameStatusMainActivityHandler,"@Rubi_Trivia_Tz");
                    }
                }
                return false;
            }
        });

        checkIfAnswersAreInTheDBReceiveAnswersGameMainActivityHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                try{
                    if(message.arg1 == 0 && message.obj == null){
                        //Answer was incorrect
                        ServerSendToDB.sendCurrentUserMostRecentRoundExcerienced(-1); // Resest the individual users round so that they are eliminated when the next round launches.
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                return false;
            }
        });

        receiveRoundFromServerHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                String roundTmp = (String) message.obj;
                if(!roundTmp.isEmpty()){
                    gameRound = roundTmp;
                    ServerReceiveFromDB.receiveCurrentUserMostRecentRoundExperienced(gameMainActivityCurrentUserMostRecentRoundExpereincedHandler);
                }
                return false;
            }
        });

        gameMainActivityCurrentUserMostRecentRoundExpereincedHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                try{
                    Integer userRound = message.arg1;
                    if(isUserInGame(userRound)){
                        // User in game
                        exchangeFragmentsDuringTransition(new QuestionsFragmentController());
                        ServerSendToDB.sendCurrentUserMostRecentRoundExcerienced(Integer.parseInt(gameRound));
                    }else{
                        //User not in game
                        exchangeFragmentsDuringTransition(new QuestionsFragmentController());
                    }
                }catch (Exception e){e.printStackTrace();}
                return false;
            }
        });

        receiveNumberOfPlayersThatAreStillInGameMainActivityHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                int numberOfPeopleInGame = message.arg1;
                numberOfPlayersStillInGame.setText(String.valueOf(numberOfPeopleInGame));
                return false;
            }
        });
    }

    private void setUpOnClickListerners(){
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MainMenuMainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void initializePlayer() {
        try{
            Intent intent = getIntent();
            //Create a default TrackSelector
            if(player == null){
                boolean preferExtensionDecoders = intent.getBooleanExtra(PREFER_EXTENSION_DECODERS, false);
                @DefaultRenderersFactory.ExtensionRendererMode int extensionRendererMode =
                        useExtensionRenderers()
                                ? (preferExtensionDecoders ? EXTENSION_RENDERER_MODE_PREFER
                                : EXTENSION_RENDERER_MODE_ON)
                                : EXTENSION_RENDERER_MODE_OFF;

                TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
                trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
                player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, new DefaultLoadControl(),
                        null, extensionRendererMode);
                playerView.setPlayer(player);
                player.setPlayWhenReady(true);
            }

            player.seekTo(0, 0);

        }catch(Exception e){e.printStackTrace();}
        try{
            player.addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    super.onPlayerStateChanged(playWhenReady, playbackState);
                    if (playWhenReady && playbackState == Player.STATE_READY) {
                        // media actually playing
                        videoProgressBar.setVisibility(View.INVISIBLE);
                    } else if (playWhenReady) {
                        // might be idle (plays after prepare()),
                        // buffering (plays when data available)
                        // or ended (plays when seek away from end)
                        videoProgressBar.setVisibility(View.VISIBLE);
                    } else {
                        // player paused in any state
                    }
                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                    player.prepare(buildMediaSource(Uri.parse(gameServer)));
                    super.onPlayerError(error);
                }
            });
        }catch(Exception e){e.printStackTrace();}
    }


    private void setUpPlayerFullScreenView(){
        if(player != null){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        }
    }

    private void getAndSetUriInPlayer(Uri uri){
        try{
            MediaSource mediaSource = buildMediaSource(uri);
            player.prepare(mediaSource, true, false);
        }catch (Exception e){e.printStackTrace();}
    }

    private void releasePlayer() {
        if (player != null) {
            player.clearVideoSurface();
            player.release();
            player = null;
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        try{
            return new HlsMediaSource.Factory(new DefaultHttpDataSourceFactory("exoplayer-codelab")).createMediaSource(uri);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }

    }

    public boolean useExtensionRenderers() {
        return BuildConfig.FLAVOR.equals("withExtensions");
    }

    private void exchangeFragmentsDuringTransition(Fragment newGenericFragment){
        try{
            if(!isFinishing()){
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                if(adapter == null){
                    adapter = new FragmentsSectionsStatePagerAdapter(fragmentManager);
                    genericPlegesFragmentLauncherAndCampaignIdSender(newGenericFragment,adapter);
                }else{
                    transaction.replace(R.id.main_view_pager_id, newGenericFragment); // Might need to check and see if there is something in the viewpager first
                    fragmentManager.popBackStack();
                    genericPlegesFragmentLauncherAndCampaignIdSender(newGenericFragment,adapter);
                    transaction.commitAllowingStateLoss(); // I do not care about the state because I have different questions coming up each time
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void genericPlegesFragmentLauncherAndCampaignIdSender(Fragment fragment, FragmentStatePagerAdapter adapter){
        FragmentsSectionsStatePagerAdapter transformAdapter = (FragmentsSectionsStatePagerAdapter) adapter;
        transformAdapter.clear();
        transformAdapter.addFragment(fragment);
        if(mViewPager != null){
            mViewPager.setAdapter(transformAdapter);
        }
    }

    private Boolean isUserInGame(Integer currentUserInteger){
            if(Integer.parseInt(gameRound) - 1 == currentUserInteger){ // If you leave for a second and come back, should not kick you off.
                // You are safe to play the game because you made it in time
                genericBooleanSetSharePreferences(myPreferencesInGame,shouldLetUserInGame,true);
                return true;
            }else{
                // You did not make it in time
                genericBooleanSetSharePreferences(myPreferencesInGame,shouldLetUserInGame,false);
                return false;
            }
    }

    private void navigateActivities(Context currentContext, Class movingClass){
        if(currentContext != null){
            Intent intent = new Intent(currentContext,movingClass);
            startActivity(intent);
            finish();
        }
    }
    private void resetSharedPreferencesForNextGame(){
        genericBooleanSetSharePreferences(myPreferencesInGame,shouldLetUserInGame,true); // Let back in game
        genericBooleanSetSharePreferences(myPreferencesExtraLives,hasShownExtraLifeKey,false);// Have not shown user extra lifes yet
    }

    //Shared preferences
    private void genericBooleanSetSharePreferences(String preferences,String key,Boolean bool){
        SharedPreferences sharedPreferences = getSharedPreferences(preferences,Context.MODE_PRIVATE);
        if(sharedPreferences != null){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(key, bool);
            editor.apply();
        }
    }
}
