package com.quiz.calvinusiri.triviaapp.Model;

public class ServerConfigStrings {
    private String UID = "uid";
    private String USER = "user";
    private String AW = "aw";
    private String R = "r";
    private String EL = "el";

    private String GAME_USERS = "game_users";
    private String GAME_QUESTIONS = "game_questions";
    private String GAME_TIMERS = "game_timers";
    private String GAME_INFO = "game_information";
    private String GRADER = "grader";
    private String GAME_SERVERS = "game_servers";
    private String GAME_ROUNDS = "game_rounds";

    private String ROUND = "round";
    private String GAMETIMER = "gameTimer";
    private String SERVER = "server";
    private String GAMEROUND = "gameRound";

    public ServerConfigStrings() {
    }

    public String getUID() {
        return UID;
    }

    public String getUSER() {
        return USER;
    }

    public String getAW() {
        return AW;
    }

    public String getR() {
        return R;
    }

    public String getGAME_USERS() {
        return GAME_USERS;
    }

    public String getGAME_QUESTIONS() {
        return GAME_QUESTIONS;
    }

    public String getGAME_TIMERS() {
        return GAME_TIMERS;
    }

    public String getGAME_INFO() {
        return GAME_INFO;
    }

    public String getGRADER() {
        return GRADER;
    }

    public String getROUND() {
        return ROUND;
    }

    public String getGAMETIMER() {
        return GAMETIMER;
    }

    public String getEL() {
        return EL;
    }

    public String getGAME_SERVERS() {
        return GAME_SERVERS;
    }

    public String getGAME_ROUNDS() {
        return GAME_ROUNDS;
    }

    public String getSERVER() {
        return SERVER;
    }

    public String getGAMEROUND() {
        return GAMEROUND;
    }
}
