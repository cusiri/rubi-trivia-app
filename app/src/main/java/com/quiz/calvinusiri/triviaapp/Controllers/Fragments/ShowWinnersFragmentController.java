package com.quiz.calvinusiri.triviaapp.Controllers.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.quiz.calvinusiri.triviaapp.Controllers.Activities.LoginMainActivity;
import com.quiz.calvinusiri.triviaapp.Model.GameInformation;
import com.quiz.calvinusiri.triviaapp.Model.ServerConfigStrings;
import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.quiz.calvinusiri.triviaapp.R;
import com.quiz.calvinusiri.triviaapp.Utils.ServerLookUpFunctions;
import com.quiz.calvinusiri.triviaapp.Utils.ServerReceiveFromDB;
import com.quiz.calvinusiri.triviaapp.Utils.ServerSendToDB;
import com.quiz.calvinusiri.triviaapp.Utils.ShowWinnersRecyclerViewAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.Query;

import java.util.ServiceConfigurationError;

public class ShowWinnersFragmentController extends Fragment {
    private ShowWinnersRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;

    private Handler ShowWinnersFragmentControllerCheckIfLoginHandler;
    private Handler receiveWinnersFromTheDBHandler;
    private Handler receiveGameInformationHandler;
    private Handler sendAmountWonToDbHandler;

    private String gamePrize;
    private static final String TAG = "LeaderboardMainActivity";
    private static final String myPreferencesInGame = "stored_preferences_in_game";
    private static final String shouldLetUserInGame = "shouldLetUserBackInGame";

    private ServerConfigStrings serverConfigStrings = new ServerConfigStrings();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_show_winners, container, false);
        recyclerView = view.findViewById(R.id.layout_show_winners_recycler_view_id);

        handleCallBacks();
        ServerReceiveFromDB.receiveGameInformationFromServer(receiveGameInformationHandler,"@Rubi_Trivia_Tz");

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        ServerLookUpFunctions.serverLookUpIfSignedIn(ShowWinnersFragmentControllerCheckIfLoginHandler);
        if (adapter != null) {
            adapter.startListening();
        }
    }
    @Override
    public void onStop () {
        super.onStop();
        if (adapter != null) {
            adapter.stopListening();
        }
    }

    // Methods

    private void handleCallBacks(){
        ShowWinnersFragmentControllerCheckIfLoginHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if (message.arg1 == 0 && getActivity() != null) {
                    Intent intent = new Intent(getContext(), LoginMainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
                return false;
            }
        });

        receiveWinnersFromTheDBHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(final Message message) {
                Query query = (Query) message.obj;
                Integer numberOfWinners = message.arg1;
                try{
                    FirestoreRecyclerOptions<Users> options = new FirestoreRecyclerOptions.Builder<Users>()
                            .setQuery(query, Users.class)
                            .build();
                    adapter = new ShowWinnersRecyclerViewAdapter(options, calculateIndividualWinnings(Integer.parseInt(gamePrize),numberOfWinners));
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    recyclerView.setAdapter(adapter);
                    adapter.startListening();
                }catch (Exception e){e.printStackTrace();}

                return false;
            }
        });

        receiveGameInformationHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                GameInformation gameInformation = (GameInformation) message.obj;
                Activity activity = getActivity();
                gamePrize = gameInformation.getGamePrize();

                if (isAdded() && activity != null){
                    if(!gamePrize.isEmpty()){
                        ServerReceiveFromDB.receiveWinnersFromDB(receiveWinnersFromTheDBHandler);
                        if(getShouldLetUserIntoGameSharePreferences(shouldLetUserInGame)){
                            // User is still in game
                            ServerSendToDB.updateUserAwards(sendAmountWonToDbHandler, serverConfigStrings.getGAME_USERS(), Integer.parseInt(gamePrize));
                            Toast.makeText(getContext(),getString(R.string.you_win) + gamePrize,Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getContext(), getString(R.string.poor_internet),Toast.LENGTH_SHORT).show();
                    }
                }

                return false;
            }
        });

        sendAmountWonToDbHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.arg1 == 0){
                    Toast.makeText(getContext(), getString(R.string.poor_internet),Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

    }

    private Integer calculateIndividualWinnings(Integer prize, Integer winners){
        return (prize/winners);
    }

    private Boolean getShouldLetUserIntoGameSharePreferences(String key){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(myPreferencesInGame, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key,false);
    }
}
