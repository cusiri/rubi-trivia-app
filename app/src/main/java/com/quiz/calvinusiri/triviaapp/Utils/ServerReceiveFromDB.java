package com.quiz.calvinusiri.triviaapp.Utils;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.QuerySnapshot;
import com.quiz.calvinusiri.triviaapp.Model.Answer;
import com.quiz.calvinusiri.triviaapp.Model.GameInformation;
import com.quiz.calvinusiri.triviaapp.Model.Questions;
import com.quiz.calvinusiri.triviaapp.Model.ServerConfigStrings;
import com.quiz.calvinusiri.triviaapp.Model.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.firestore.FirebaseFirestore;


import java.util.HashMap;
import java.util.Map;

public class ServerReceiveFromDB {
    // RECEIVE FROM SERVER
    private static FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private static FirebaseStorage storage = FirebaseStorage.getInstance();
    private static FirebaseDatabase database = FirebaseDatabase.getInstance();
    private static FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    private static FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
            .build();
    private static final String TAG = "ServerReceiveFromDB";
    private static ServerConfigStrings serverConfigStrings = new ServerConfigStrings();

    // Get all game information at once
    public static void receiveGameInformationFromServer(final Handler handler,String game){
        try{
            database.getReference().child(serverConfigStrings.getGAME_INFO()).child(game).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    GameInformation gameInformation = dataSnapshot.getValue(GameInformation.class);
                    sendMessageWithObj(gameInformation,handler);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }catch (Exception e){ e.printStackTrace(); }
    }

    //Since game time is denomlaized, it needs its own method
    public static void receiveGameTimer(final Handler handler,String game){
        try{
            database.getReference().child(serverConfigStrings.getGAME_TIMERS()).child(game).child(serverConfigStrings.getGAMETIMER()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Long gameT = dataSnapshot.getValue(Long.class);
                    sendMessageWithObj(gameT,handler);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }catch (Exception e){ e.printStackTrace(); }
    }

    // Receive information of the current user
    public static void receiveLoggedInUserInformationFromServer(final Handler handler){
        if(mAuth.getCurrentUser() != null){
            firestore.setFirestoreSettings(settings);
            firestore.collection(serverConfigStrings.getGAME_USERS()).document(mAuth.getCurrentUser().getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                    if(documentSnapshot.exists()){
                        Map userMap = new HashMap();
                        Users users = documentSnapshot.toObject(Users.class);
                        userMap.put(serverConfigStrings.getUID(),mAuth.getCurrentUser().getUid());
                        userMap.put(serverConfigStrings.getUSER(),users);
                        sendMessageWithObj(userMap,handler);
                    }else{
                        sendMessageWithObj(null,handler);
                    }
                }
            });
        }
    }
    // Recieve singleEvent Listerner user information from db
    public static void receiveLoggedInUserInformationFromServerOnce(final Handler handler){
        if(mAuth.getCurrentUser() != null){
            firestore.setFirestoreSettings(settings);
            firestore.collection(serverConfigStrings.getGAME_USERS()).document(mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){
                        DocumentSnapshot documentSnapshot = task.getResult();
                        Map userMap = new HashMap();
                        Users users = documentSnapshot.toObject(Users.class);
                        userMap.put(serverConfigStrings.getUID(),mAuth.getCurrentUser().getUid());
                        userMap.put(serverConfigStrings.getUSER(),users);
                        sendMessageWithObj(userMap,handler);
                    }else{
                        sendMessageWithObj(null,handler);
                    }
                }
            });
        }
    }

    public static void receiveGameQuestionsFromTheDb(final Handler handler, Integer round, String game){
        database.getReference().child(serverConfigStrings.getGAME_QUESTIONS()).child(game).orderByChild(serverConfigStrings.getROUND()).equalTo(String.valueOf(round)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    Questions questions = postSnapshot.getValue(Questions.class);
                    sendMessageWithObj(questions, handler);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    // This also needs to be a one time look up, received from the server
    // Gets the current users information once, which is needed. we dont want a listener here
    public static void receiveCurrentUserMostRecentRoundExperienced(final Handler handler){
        if(mAuth.getCurrentUser() != null ){
            firestore.collection(serverConfigStrings.getGAME_USERS()).document(mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){
                        Users users = task.getResult().toObject(Users.class);
                        sendMessageWithArgAlone(users.getR(), handler);
                    }
                }
            }); // This will only get data once, will not add a listener
        }
    }

    // Single listener for a single call to the server
    public static void receiveRoundFromServer(final Handler hander, String parent, String game, String child){
        if(mAuth.getCurrentUser() != null){
            database.getReference().child(parent).child(game).child(child).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String round = dataSnapshot.getValue(String.class);
                    sendMessageWithObj(round,hander);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public static void genericReceiveString(final Handler handler, String parent, String child, String game){
        if(mAuth.getCurrentUser() != null){
            database.getReference().child(parent).child(game).child(child).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String genericString = dataSnapshot.getValue(String.class);
                    sendMessageWithObj(genericString,handler);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    // QUERRIES
    public static void receiveWinnersFromDB(final Handler handler){
        final Query query = firestore.collection(serverConfigStrings.getGAME_USERS()).whereGreaterThan("r",-1);
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                sendMessageWithArgAndObj(queryDocumentSnapshots.getDocuments().size(),query,handler);
            }
        });

    }
    public static void receivePlayersStillInGameFromDB(final Handler handler){
        firestore.collection(serverConfigStrings.getGAME_USERS())
                .whereGreaterThan(serverConfigStrings.getR(), -1)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "listen:error", e);
                            return;
                        }
                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:
                                    sendMessageWithArgAlone(snapshots.getDocuments().size(),handler);
                                    break;
                                case MODIFIED:
                                    sendMessageWithArgAlone(snapshots.getDocuments().size(),handler);
                                    break;
                                case REMOVED:
                                    sendMessageWithArgAlone(snapshots.getDocuments().size(),handler);
                                    break;
                            }
                        }

                    }
                });

    }

    public static void queryAndOrderUsersFromDb(Handler handler){
        Query query = firestore.collection(serverConfigStrings.getGAME_USERS()).orderBy(serverConfigStrings.getAW(), Query.Direction.DESCENDING);
        sendMessageWithObj(query,handler);

    }



    public static void queryRoundToGetAnswerFromDB(final Handler handler,String game, String round){
        database.getReference().child(serverConfigStrings.getGRADER()).child(game).orderByChild(serverConfigStrings.getROUND()).equalTo(String.valueOf(round)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    Answer answer = postSnapshot.getValue(Answer.class);
                    sendMessageWithObj(answer, handler);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    // Utils
    private static void sendMessageWithObj(Object obj, Handler handler){
        Message message = handler.obtainMessage();
        message.obj = obj;
        handler.sendMessage(message);
    }

    private static void sendMessageWithArgAlone(int arg,Handler handler){
        Message message = handler.obtainMessage();
        message.arg1 = arg;
        handler.sendMessage(message);
    }

    private static void sendMessageWithArgAndObj(int arg,Object obj, Handler handler){
        Message message = handler.obtainMessage();
        message.obj = obj;
        message.arg1 = arg;
        handler.sendMessage(message);
    }
}
