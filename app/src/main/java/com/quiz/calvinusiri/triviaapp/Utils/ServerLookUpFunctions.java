package com.quiz.calvinusiri.triviaapp.Utils;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.quiz.calvinusiri.triviaapp.Model.Answer;
import com.quiz.calvinusiri.triviaapp.Model.Questions;
import com.quiz.calvinusiri.triviaapp.Model.ServerConfigStrings;


import static com.quiz.calvinusiri.triviaapp.Controllers.Activities.GameMainActivity.checkIfAnswersAreInTheDBReceiveAnswersGameMainActivityHandler;

public class ServerLookUpFunctions {
    private static FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private static FirebaseDatabase database = FirebaseDatabase.getInstance();
    private static final String TAG = "ServerLookUpFunctions";
    private static ServerConfigStrings serverConfigStrings = new ServerConfigStrings();

    //LOOKUP TO SERVER
    public static void serverLookUpLogIn(final Handler handler, String email, String password){
        try{
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        sendMessageWithArgAlone(1,handler);
                    }else {
                        sendMessageWithArgAlone(0,handler);
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void serverLookUpIfSignUpSuccessful(String email, String password, final Handler handler){
        try{
            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        sendMessageWithArgAlone(1,handler);
                    }else{
                        sendMessageWithArgAlone(0,handler);
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void serverLookUpIfSignedIn(Handler handler){
        if(mAuth.getCurrentUser() != null){
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if(currentUser == null){
                ServerSendToDB.sendUpdateUserLogOut();
                sendMessageWithArgAlone(0,handler);
            }else{
                sendMessageWithArgAlone(1,handler);
            }
        }
    }


    public static void serverLookUpIfTheAnswerIsInTheDataBase(final String answerSelected, String game, Integer round, final Handler handler){
        database.getReference().child(serverConfigStrings.getGRADER()).child(game).orderByChild(String.valueOf(round)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    Answer answer = postSnapshot.getValue(Answer.class); // Create new variable in memory for security reasons
                    String answersSelectedString = answerSelected; // Create new variable in memory for security reasons
                    if (new String(answersSelectedString).equals(answer.getAnswer())) {
                        sendMessageWithArgAlone(1, handler);// True
                    } else {
                        sendMessageWithArgAlone(0, handler);// False
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // Utils
    private static void sendMessageWithArgAlone(int arg, Handler handler){
        Message message = handler.obtainMessage();
        message.arg1 = arg;
        handler.sendMessage(message);
    }
}
