package com.quiz.calvinusiri.triviaapp.Model;

import java.util.List;

public class Users {
    private String e; // email
    private Long aw;// amount won
    private Integer r; //roundThatTheUserWasMostRecentlyInBeforeUpdate
    private List<String> s; // games subscribed to
    private Boolean el; // Extra lives, true or false
    private String ref; // Were you refered by another player, if so, save the ref code here

    Users(){}

    public Users(String email, Long aw, String referal, Boolean extraLife, Integer round, List<String> subscribers) {
        this.e = email;
        this.aw = aw;
        this.ref = referal;
        this.el = extraLife;
        this.r = round;
        this.s = subscribers;
    }

    public String getE() {
        return e;
    }

    public List<String> getS() {
        return s;
    }

    public Long getAw() {
        return aw;
    }

    public Integer getR() { return r; }

    public void setR(Integer r) { this.r = r; }

    public Boolean getel() {
        return el;
    }

    public String getRef() {
        return ref;
    }

    public String setEmailAsReference(String emailRef){
        return emailRef.split("@")[0];
    }
}
