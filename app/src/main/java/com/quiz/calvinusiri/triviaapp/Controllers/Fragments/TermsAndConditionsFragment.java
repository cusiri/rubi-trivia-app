package com.quiz.calvinusiri.triviaapp.Controllers.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quiz.calvinusiri.triviaapp.R;

public class TermsAndConditionsFragment extends Fragment {
    TextView legalText;
    TextView legalTitle;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tnc_layout_layout,container,false);
        legalText = view.findViewById(R.id.generic_text_in_scroll_view_text_view_id);
        legalTitle = view.findViewById(R.id.generic_title_text_view_id);
        setUpUi("QX Trivia Terms and Conditions");
        return view;
    }

    private void setUpUi(final String url){

        SpannableString ss = new SpannableString(url);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1-CwwhH7msnY_mWf6JE_Cm3hlmtegLxhztYdehrjsvPE/edit?usp=sharing"));
                startActivity(browserIntent);
            }
        };
        ss.setSpan(clickableSpan,0,url.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        legalText.setText(ss);
        legalText.setMovementMethod(LinkMovementMethod.getInstance());
        legalTitle.setText(getString(R.string.official));
    }
}
